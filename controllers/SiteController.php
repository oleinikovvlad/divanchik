<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new ContactForm();
        
        if ($model->load(Yii::$app->request->post())){
            if ($model->validate()){
                $model->status = 0;
                Yii::$app->session->setflash('sendMail', 'Спасибо за обращение');
                return $this->refresh();
            } 
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

}
