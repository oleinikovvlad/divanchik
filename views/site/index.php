<?php

/* @var $this yii\web\View */
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Divanchik.РУ';
?>
    <div class="message shadow">
       
        <input hidden type="checkbox" id="show-form"  <?php echo(($model->status) ? 'checked="checked"' : '') ?>>
        <?php $form = ActiveForm::begin(['options'=> ['class'=>'form']]); ?>

        <?= $form->field($model, 'customer_name') ?>
        <?= $form->field($model, 'phone')?>
        <?= Html::submitButton('Обратная связь') ?>
        <?php ActiveForm::end();?>
        <div class="">
            <label class="show-form" for="show-form"></label>
            <label class="hide-form" for="show-form"></label>
            <div style="text-align:center;"><?php 
                if( Yii::$app->session->hasFlash('sendMail') ) :
                    echo Yii::$app->session->getFlash('sendMail');
                endif; ?></div> 
        </div>
              
        
    </div>
    <div class="wrap">
        <div class="head">
            <input type="radio" hidden name="img" id="mini-img-v1" checked="checked">
            <input type="radio" hidden name="img" id="mini-img-v2">
            <div class="main-photo">                
                <div class="img" id="img-v1"></div>
                <div class="img" id="img-v2"></div>
            </div>
            <div class="cameo-photo">
                <label for="mini-img-v1" class="mini-img" id="mini-v1"></label>
                <label for="mini-img-v2" class="mini-img" id="mini-v2"></label>
            </div>
        </div>
        <div class="content">
            <input type="radio" hidden name="nav" id="description" checked="checked">
            <input type="radio" hidden name="nav" id="delivery">
            <input type="radio" hidden name="nav" id="about">
            <div class="tabs  ">
                <nav>
                    <label class="nav description" for="description">Описание</label>
                    <label class="nav delivery" for="delivery">Доставка и оплата </label>
                    <label class="nav about" for="about">Почему Диванчик.ру? </label>
                </nav>
                <div class="container description" >
                    <div class="tab  ">
                        <section>
                            <p>
                                Это кресло не пугает время, погодные условия или другие природные катаклизмы.
                            </p>
                            <p>
                                Единсвенное что на него может повлять, это пламя огнедышащего дракона. Но последний из них улетел много веков назад в неизвестном направлении, и возвращаться не собирается.
                            </p>
                        </section>
                        <section class="">
                            <div class="description">
                                <div class="des-img"></div>
                                <div>
                                    <div>Английский колорит</div>
                                    <div class="text-descr">
                                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestiae corporis quia non dolore mollitia a?
                                    </div>
                                </div>
                            </div>
                            <div class="description">
                                <div class="des-img"></div>
                                <div>
                                    <div>Английский колорит</div>
                                    <div class="text-descr">
                                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestiae corporis quia non dolore mollitia a?
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente porro quibusdam delectus perspiciatis, aliquid nostrum dolore quia aliquam quae nemo ex! Sunt minus nisi odit tempore sit voluptatibus cum quod!
                            </p>
                        </section>

                    </div>
                    <section>
                        <h3>С этим товаром часто покупают:</h3>
                        <div class=" ">
                            <div class="cards">
                                <div class="card v1 ring">Кольцо Всевластия</div>
                                <div class="card v2 back">Это кольцо даст безграничную силу его владельцу.</div>
                            </div>
                            <div class="cards">
                                <div class="card v1 glove">Перчатка таноса</div>
                                <div class="card v2 back">
                                    <div>"Щелк" и все...</div>
                                    <div>P.S. Камни продаются отдельно.</div>
                                </div>
                            </div>
                            <div class="cards">
                                <div class="card v1 ring">Кольцо Всевластия</div>
                                <div class="card v2 back">Lorem ipsum dolor sit amet consectetur, adipisicing elit. </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="container delivery" >
                    <div class="tab">
                        <section>
                            <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Modi, ea. Dolorem doloremque ipsam deserunt, accusamus nemo pariatur ullam fuga amet architecto adipisci odio dolor corrupti, aliquid rerum itaque quasi! Consectetur!
                            </p>
                            <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Modi, ea. Dolorem doloremque ipsam deserunt, accusamus nemo pariatur ullam fuga amet architecto adipisci odio dolor corrupti, aliquid rerum itaque quasi! Consectetur!
                            </p>
                            <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Modi, ea. Dolorem doloremque ipsam deserunt, accusamus nemo pariatur ullam fuga amet architecto adipisci odio dolor corrupti, aliquid rerum itaque quasi! Consectetur!
                            </p>
                        </section>
                    </div>
                    <section class=" ">
                        <h3>Этот товаром можно доставить:</h3>
                        <div class="  ">
                            <div class="cards-v1">
                                <div class="card v1 dove">Голубем</div>
                                <div class="card v2 back">
                                    <div>Конструктор лего, с доставка до окна.</div>
                                    <div>Будет чем заняться несколько недель :)</div>
                                </div>
                            </div>
                            <div class="cards-v1">
                                <div class="card v1 courier">Курьером</div>
                                <div class="card v2 back">
                                    <div>Доставка до подъезда, поднимать придется самому.</div>
                                    <div>Время сборки 2-3 часа.</div>
                                </div>
                            </div>
                            <div class="cards-v1">
                                <div class="card v1 cart">Повозкой</div>
                                <div class="card v2 back">Упакуют.<br>Доставят.<br>Поднимут.<br>Установят.<br>Посадят.</div>
                            </div>
                        </div>
                    </section>

                </div>
                <div class="container about" >
                    <div class="tab">
                        <section>
                            <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Modi, ea. Dolorem doloremque ipsam deserunt, accusamus nemo pariatur ullam fuga amet architecto adipisci odio dolor corrupti, aliquid rerum itaque quasi! Consectetur!
                            </p>
                            <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Modi, ea. Dolorem doloremque ipsam deserunt, accusamus nemo pariatur ullam fuga amet architecto adipisci odio dolor corrupti, aliquid rerum itaque quasi! Consectetur!
                            </p>
                            <p>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Modi, ea. Dolorem doloremque ipsam deserunt, accusamus nemo pariatur ullam fuga amet architecto adipisci odio dolor corrupti, aliquid rerum itaque quasi! Consectetur!
                            </p>
                        </section>
                    </div>
                </div>
            </div>
            <aside class="buy-control shadow">
                <div class="title">
                    <h3>Кресло</h3><h3>"Трон престолов"</h3>
                    <div class="buttons">
                        <button class="button shadow">В корзину</button>
                        <button class="button shadow">Купить в 1 клик</button>
                    </div>
                    
                </div>
                <div class="count   ">Этот предмет купили: 1 раз!</div>
                <div class="description  ">
                    <h3>Конфигурация</h3>
                    <div>
                        <h4>Размер</h4>
                        <div class="d-center">
                            <div class="d-inline">
                                <div class="d-inline">
                                    <div>длина</div>
                                    <div>75см</div>
                                </div>
                            </div>
                            <span class="d-inline">x</span>
                            <div class="d-inline">
                                <div class="">
                                    <div>ширина</div>
                                    <div>75см</div>
                                </div>
                            </div>
                            <span class="d-inline">x</span>
                            <div class="d-inline">
                                <div class="">
                                    <div>высота</div>
                                    <div>150см</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <h4>Посадочное место</h4>
                        <div class="d-center">
                            <div class="d-inline">
                                <div class=" ">
                                    <div>глубина</div>
                                    <div>55см</div>
                                </div>
                            </div>
                            <span class="d-inline">x</span>
                            <div class="d-inline">
                                <div class=" ">
                                    <div>высота</div>
                                    <div>55см</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside> 
        </div>
        <footer>
            <div class="cards">
                <div class="card v1 back">Карточка</div>
                <div class="card v2 back">Описание карточки</div>
            </div>
            <div class="cards">
                <div class="card v1 back">Карточка</div>
                <div class="card v2 back">Описание карточки</div>
            </div>
            <div class="cards">
                <div class="card v1 back">Карточка</div>
                <div class="card v2 back">Описание карточки</div>
            </div>
            <div class="cards">
                <div class="card v1 back">Карточка</div>
                <div class="card v2 back">Описание карточки</div>
            </div>
        </footer>
    </div>    
