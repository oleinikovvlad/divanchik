<?php

namespace app\models;

use yii\base\Model;

class ContactForm extends Model
{
    public $customer_name ;
    public $phone;
    public $status = 0;

    
    public function rules()
    {
        return [
            [['customer_name', 'phone'],'trim'],
            ['phone', 'required' ],
            ['phone', 'string','length'=>[11,16] ],
            ['customer_name', 'string','max'=>256 ],
            ['phone', 'maskPhone'],
        ];
    }

    public function maskPhone($phone)
    {    
        $reg = '/^[+]*[7-8][-(]*[0-9]{3}[-)]*[0-9]{3}[-]*[0-9]{2}[-]*[0-9]{2}/';
        if(preg_match( $reg, $this->$phone)){
            $this->status = 0;
        } else {
            $this->addError($phone, 'Не верный номер');
            $this->status = 1;
        }

    }

    public function attributeLabels()
    {
        return [
            'customer_name' => 'Имя',
            'phone' => 'Телефон',
        ];
    }

}
